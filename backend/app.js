var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var bdRouter = require('./routes/bd');
var charsRouter = require('./routes/characters');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/loadInfo', bdRouter);
app.use('/characters', charsRouter);

module.exports = app;
