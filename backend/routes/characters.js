var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function (req, res, next) {
  res.json({ estado: "ok" });
});
router.get("/:id", function (req, res, next) {
  console.log(req.params);
  res.json({ estado: "ok", id: req.params.id });
});

module.exports = router;
