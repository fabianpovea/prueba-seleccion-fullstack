import React from "react";
import { useHistory } from "react-router-dom";
import "../../css/css.css"

const TableData = ({ data }) => {
  let history = useHistory();
  const cargaDatos = (id) => {
    history.push(`/view/${id}`);
  };
  return data ? (
    <table>
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Sexo</th>
          <th>Casa</th>
          <th>Cultura</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {!data
          ? null
          : data.map((dato) => (
              <tr key={dato.id}>
                <td>{dato.name}</td>
                <td>{dato.gender}</td>
                <td>{dato.house}</td>
                <td>{dato.culture}</td>
                <td>
                  <button onClick={() => cargaDatos(dato.id)}>Más datos</button>
                </td>
              </tr>
            ))}
      </tbody>
    </table>
  ) : (
    <label>Cargando...</label>
  );
};

export default TableData;
