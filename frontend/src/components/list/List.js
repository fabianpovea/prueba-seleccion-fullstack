import React, { useState, useEffect, Fragment } from "react";
import clienteAxios from "../../config/back";
import TableData from "./Table";
import Search from "./Search";
import "../../css/css.css"


const List = () => {
  const [datos, setDatos] = useState();
  const [pagina, setPagina] = useState(1);
  const [cantPag] = useState(10);
  const [infoPaginacion, setInfoPaginacion] = useState({
    hasNextPage: false,
    hasPrevPage: false,
    totalPage: 0,
    totalDocs: 0,
  });
  const [datosBuscar, setDatosBuscar] = useState({
    tipo: 1,
    valor: "",
  });

  useEffect(() => {
    var traeDatos = async () => {
      setDatos();
      if (datosBuscar.tipo && datosBuscar.valor) {  
        let arrDatos = await clienteAxios.get(
          `/characters?pagina=${pagina}&cantPag=${cantPag}&tipo=${datosBuscar.tipo}&valor=${datosBuscar.valor}`
        );
        if (arrDatos.status === 200 && arrDatos.data.data) {
          setDatos(arrDatos.data.data.docs);
          setInfoPaginacion({
            hasNextPage: arrDatos.data.data.hasNextPage,
            hasPrevPage: arrDatos.data.data.hasPrevPage,
            totalPage: arrDatos.data.data.totalPage,
            totalDocs: arrDatos.data.data.totalDocs,
          });
        } else {
          setDatos();
          setInfoPaginacion({
            hasNextPage: false,
            hasPrevPage: false,
            totalPage: 0,
            totalDocs: 0,
          });
        }
      } else {
        let arrDatos = await clienteAxios.get(
          `/characters?pagina=${pagina}&cantPag=${cantPag}`
        );
        if (arrDatos.status === 200 && arrDatos.data.data) {
          setDatos(arrDatos.data.data.docs);
          setInfoPaginacion({
            hasNextPage: arrDatos.data.data.hasNextPage,
            hasPrevPage: arrDatos.data.data.hasPrevPage,
            totalPage: arrDatos.data.data.totalPage,
            totalDocs: arrDatos.data.data.totalDocs,
          });
        } else setDatos();
      }
    };
    traeDatos();
  }, [pagina, cantPag, datosBuscar]);

  return (
    <Fragment>
      <Search setDatosBuscar={setDatosBuscar} />
      <TableData data={datos}/>
      <button
        disabled={!infoPaginacion.hasPrevPage}
        onClick={() => setPagina(pagina - 1)}
      >
        {"<"}
      </button>
      <button
        disabled={!infoPaginacion.hasNextPage}
        onClick={() => setPagina(pagina + 1)}
      >
        {">"}
      </button>
    </Fragment>
  );
};

export default List;
