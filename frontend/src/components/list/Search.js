import React, { useState } from "react";

const Search = ({ setDatosBuscar }) => {
  const [tipo, setTipo] = useState(1);
  const [valor, setValor] = useState("");

  const enviaDatosBuscar = () => {
    setDatosBuscar({ tipo, valor });
  };
  const limpiaBusqueda = () => {
    setDatosBuscar({});
  };
  return (
    <form>
      <label>
        Buscar por:
        <select
          name="select"
          onChange={(e) => {
            setTipo(e.target.value);
          }}
        >
          <option value={1}>Nombre</option>
          <option value={2}>Casa</option>
        </select>
      </label>
      <input
        type="text"
        name="valor"
        onChange={(e) => {
          setValor(e.target.value);
        }}
      />
      <input
        type="button"
        value="Buscar"
        onClick={() => {
          enviaDatosBuscar();
        }}
      />
      <input
        type="button"
        value="Reestablecer"
        onClick={() => {
          limpiaBusqueda();
        }}
      />
    </form>
  );
};

export default Search;
