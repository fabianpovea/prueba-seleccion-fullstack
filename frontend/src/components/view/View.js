import React, { useEffect, useState } from "react";
import clienteAxios from "../../config/back";

import { useParams } from "react-router-dom";

const List = () => {
  const [inforChar, setInfoChar] = useState({});
  const [loading, setLoading] = useState(true);
  let { ID } = useParams();

  useEffect(() => {
    const traeData = async () => {
      const arrChar = await clienteAxios.get(`/characters/${ID}`);
      if (arrChar.status === 200 && arrChar.data.data) {
        setInfoChar(arrChar.data.data);
        setLoading(false);
      }
    };
    traeData();
  }, [ID]);

  return loading ? (
    <label>Cargando...</label>
  ) : (
    <div>
      <table>
        <thead>
          <tr>
            <th></th>
            <th>Nombre</th>
            <th>Casa</th>
            <th>Sexo</th>
            <th>Slug</th>
            <th>Ranking</th>
            <th>Hijos</th>
            <th>Titulos</th>
            <th>Libros</th>
          </tr>
        </thead>
        <tbody>
          {inforChar && (
            <tr>
              <td>
                <img
                  src={inforChar.image}
                  width="200"
                  height="200"
                  alt={inforChar.name}
                />
              </td>
              <td>{inforChar.name}</td>
              <td>{inforChar.house}</td>
              <td>{inforChar.gender}</td>
              <td>{inforChar.slug}</td>
              <td>{inforChar.pagerank.rank}</td>
              <td>
                <ul>
                  {inforChar.children &&
                    inforChar.children.map((chld_name) => (
                      <li
                        style={{ listStyleType: "none" }}
                        key={`${chld_name}`}
                      >
                        {chld_name}
                      </li>
                    ))}
                </ul>
              </td>
              <td>
                <ul>
                  {inforChar.titles &&
                    inforChar.titles.map((title) => (
                      <li style={{ listStyleType: "none" }} key={`${title}`}>
                        {title}
                      </li>
                    ))}
                </ul>
              </td>
              <td>
                <ul>
                  {inforChar.books &&
                    inforChar.books.map((book) => (
                      <li style={{ listStyleType: "none" }} key={`${book}`}>
                        {book}
                      </li>
                    ))}
                </ul>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default List;
