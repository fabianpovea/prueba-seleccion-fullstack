import React from "react";

import { BrowserRouter, Switch, Route } from "react-router-dom";
import List from "../list/List";
import View from "../view/View";
import Inicio from "../inicio/Inicio";
import RoutingList from "./RoutingList";

const Routing = () => {
  return (
    <div>
      <BrowserRouter>
        <RoutingList />

        <Switch>
          <Route exact path="/" component={Inicio} />
          <Route exact path="/list" component={List} />
          <Route exact path="/view/:ID" component={View} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default Routing;
