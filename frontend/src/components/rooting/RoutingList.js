import React from "react";
import { Link } from "react-router-dom";

const RoutingList = () => {
  return (
    <ul style={{ listStyleType: "none", display: "flex" }}>
      <li>
        <Link to="/" style={{ marginRight: "10px" }}>
          Inicio
        </Link>
      </li>
      <li>
        <Link to="/list" style={{ marginRight: "10px" }}>
          Listado
        </Link>
      </li>
      {/* <li>
        <Link to="/view" style={{ marginRight: "10px" }}>
          Ver
        </Link>
      </li> */}
    </ul>
  );
};

export default RoutingList;
